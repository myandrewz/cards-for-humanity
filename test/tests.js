//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
const expect = require('chai').expect;

chai.use(chaiHttp);

describe('Authenticate', () => {

    let user = {
        'username': 'myandrewz@gmail.com',
        'password': 'maureen'
    }

    describe('Login User', () => {
        it(' Should login the user ', (done) => {
            chai.request(server)
                .post('/api/authenticate')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('token');
                    done();
                });
        });
    });
    describe('register a new user', () => {
        it('Should signup the user ', (done) => {
            let user = {
                name: "Testing User",
                username: "testing@test.com",
                password: "suspect_20167",

            }
            chai.request(server)
                .post('/api/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    done();
                });
        });
    });

});