var express = require('express');
var router = express.Router();
var mongodb = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var url = 'mongodb://localhost:27017/test'
var passport = require('passport');
var passportLocal = require('passport-local');
var bCrypt = require('bcrypt-nodejs')

var jwt = require('jsonwebtoken');

var jwtOptions = {}
jwtOptions.secretOrKey = 'tasmanianDevil';

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    'name': { type: String, required: false },
    'username': { type: String, required: false },
    'password': String,

}, { collection: 'users' })

var userModel = mongoose.model('userModel', userSchema)

// checking if password is valid
var validPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
};


passport.use(new passportLocal.Strategy(function(username, password, done) {

    userModel.findOne({ 'username': username },
        function(err, user) {
            // In case of any error, return using the done method
            if (err)
                return done(err);
            // Username does not exist, log error & redirect back
            if (!user) {
                console.log('User Not Found with username ' + username);
                return done(null, false);
            }
            // User exists but wrong password, log the error 
            if (!validPassword(user, password)) {
                console.log('Invalid Password');
                return done(null, false);
            }

            done(null, user);
        }
    );

}))

passport.serializeUser(function(user, done) {
    done(null, user.id);
})

passport.deserializeUser(function(id, done) {
    done(null, { id: id, name: id });
})



/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendfile('./public/views/index.html', { isAuthenticated: req.isAuthenticated() });
});

router.get('/api/authors', function(req, res, next) {
    mongodb.connect(url, (error, client) => {
        var authors = [];
        if (error) return console.log('this is an error' + error)
        database = client.db('test');

        // function to get all items
        userModel.find().then(response => {
            res.json(response)
        });



    })
});

router.post('/api/authenticate', passport.authenticate('local'), function(req, res, next) {

    const token = jwt.sign(req.user.toJSON(), jwtOptions.secretOrKey, { expiresIn: 604800 });
    res.send({
        isAuthenticated: req.isAuthenticated(),
        user: req.user,
        token: token
    });
});

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

router.post('/api/signup', function(req, res, next) {

    var data = new userModel({
        'name': req.body.name,
        'username': req.body.username,
        'password': createHash(req.body.password)
    })

    data.save().then(response => {
            const token = jwt.sign(response.toJSON(), jwtOptions.secretOrKey, { expiresIn: 604800 });
            res.json({
                message: 'OK',
                user: response,
                token: token
            })
        },
        err => {
            console.log(err)
        });



});


module.exports = router;